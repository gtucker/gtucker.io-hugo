---
author: "Guillaume Tucker"
title: "Testing Chromebooks with LAVA on kernelci.org"
date: 2018-08-29
tags: ['Software', 'Linux', 'KernelCI', 'Chromebook', 'Collabora']
canonicalUrl: "https://www.collabora.com/news-and-blog/blog/2018/08/29/testing-chromebooks-with-lava-on-kernelci/"
description: "Booting a Chromebook with Depthcharge and automating it with LAVA for kernelci.org"
---

## Some context

You might have heard of the [Google
Chromebook](https://www.google.com/intl/en_uk/chromebook/) laptops. They come
with Chrome OS, to run applications in the Chrome web browser such as Gmail,
YouTube, Google Docs, Google Drive etc. [Chromium
OS](https://www.chromium.org/chromium-os) is the open-source project behind
Chrome OS, based on Linux (Gentoo). As part of the effort to keep the mainline
Linux kernel working on these devices, they are being continuously tested on
kernelci.org.

[kernelci.org](https://kernelci.org/) is a project dedicated to testing the
mainline Linux kernel in order to find issues introduced during its
development. It uses [LAVA](https://lavasoftware.org/) to run tests on a
variety of platforms in many different test labs as explained on the
kernelci.org [wiki](https://github.com/kernelci/kernelci-doc/wiki/KernelCI).

I've been enabling several Chromebooks to be tested on kernelci.org as part of
my work at [Collabora](https://collabora.com/). There are quite a few steps to
go through in order to be able to achieve this, so let's start from the
beginning.


## Prerequisites

In order to put the information below into practice, you'll need:

* a Chromebook device with a compatible [Servo debug
  board](https://www.chromium.org/chromium-os/servo)
* a Linux PC to build the Chromebook bootloader from source
* optionally a LAVA installation in order to automate running things on the
  device

It can be difficult to find a Servo debug board, but there are
alternatives. Its PCB design is open source so you can in principle get one
made. Some Servo boards will only work with Chromebook devices that have a
special debug connector fitted on the motherboard. Newer Chromebooks can
apparently be used with USB Type-C debug interfaces, although I haven't tried
to do this myself.

The part about [LAVA
automation](https://gtucker.io/2018/08/29/chromebooks-depthcharge-lava-kernelci/#chromebooks-in-lava)
is useful for continuous testing, but for kernel development it can be easier
to configure the device to load a kernel image from a fixed network location
and have direct console interaction. The first part about [Depthcharge with
tftpboot](https://gtucker.io/2018/08/29/chromebooks-depthcharge-lava-kernelci/#depthcharge)
is relevant in this case.


## Depthcharge with tftpboot

The part of Chrome OS that loads the Linux kernel and starts the device is
called [Depthcharge](https://www.youtube.com/watch?v=6ZKeDGI75vw) ([source
code](https://chromium.googlesource.com/chromiumos/platform/depthcharge/)). It's
a payload for the [Coreboot](https://www.coreboot.org/) bootloader. So the
ideal way to boot a Chromebook is to use Depthcharge. For development and
testing, it can download a Linux kernel image over the network (TFTP) by
enabling a debug command line. This is how LAVA controls Chromebooks, an
example of which can be seen in [Collabora's LAVA
lab](https://lava.collabora.co.uk/scheduler/job/4346862).

The first step is to rebuild Depthcharge with the debug command line interface
enabled, in order to be able to dynamically download a Linux kernel image using
TFTP and boot it.

Here's a summary of the steps to follow to build this from source:

* Get the Chromium OS source code and set up the build environment as per the
  [quick start guide](https://www.chromium.org/chromium-os/quick-start-guide)
* Don't build all of Chromium OS! You can, but it's very large and we're only
  interested in the bootloader part here.
* Find out the code name for the device you have as per the [developer
  information
  page](https://www.chromium.org/chromium-os/developer-information-for-chrome-os-devices).
* Enter the source code tree and build the firmware for your device.  Here are
  some sample commands for the [Samsung Chromebook
  Plus](https://www.google.com/chromebook/samsung-chromebook-plus/), code named
  "gru-kevin":

```
cd chromeos
export PATH=$PWD/chromium/tools/depot_tools:$PATH
cros_sdk --enter --no-ns-pid
export ACCEPT_LICENSE=Google-TOS
export BOARD=gru
./setup_board --board=$BOARD
USE=fwconsole emerge-${BOARD} \
  libpayload \
  depthcharge \
  coreboot \
  vboot_reference \
  chromeos-bootimage
```

The "USE=fwconsole" option enables the console in Depthcharge.

I've done this for a few devices, so alternatively the build step can be
skipped by downloading one of these binaries:

* [depthcharge-rk3399-gru-kevin-20180806.dev.bin](https://images.collabora.co.uk/lava/boot/rk3399-gru-kevin/depthcharge-rk3399-gru-kevin-20180806.dev.bin)
* [depthcharge-rk3288-veyron-jaq-20180803.dev.bin](https://images.collabora.co.uk/lava/boot/rk3288-veyron-jaq/depthcharge-rk3288-veyron-jaq-20180803.dev.bin)
* [depthcharge-tegra124-nyan-big-20180712.dev.bin](https://images.collabora.co.uk/lava/boot/tegra124-nyan-big/depthcharge-tegra124-nyan-big-20180712.dev.bin)

Once the build is complete (or a compatible binary has been downloaded) the
firmware can be flashed onto the device. Each device type requires a slightly
different flashing method, so here's an example for the same "gru-kevin"
device: [flash-kevin.sh](/flash-kevin.sh). It will first read the current
firmware and save it in a file, to be able to restore it later on if there was
any problem with the new firmware.

When the device boots, the CPU serial console should show a prompt. Here's a
typical command to boot over TFTP with the kernel command line stored in a
file:

```Starting depthcharge on gru...
gru: tftpboot 192.168.0.123 image.itb cmdline
```

The "image.dtb" file is a Linux kernel image in the FIT format. It can be
created with a regular "mkimage" command:

```
mkimage \
  -D "-I dts -O dtb -p 2048" \
  -f auto \
  -A arm \
  -O linux \
  -T kernel \
  -C None \
  -d Image \
  -a 0 \
  -b rk3399-gru-kevin.dtb \
  -i ramdisk.cpio.gz \
  image.itb
```

The "cmdline" file simply contains the kernel command line, here's an example:

```
earlyprintk=ttyS2,115200n8
console=tty1
console=ttyS2,115200n8
root=/dev/ram0
ip=dhcp
```

## Chromebooks in LAVA

This can now all be automated in LAVA. Some device types such as the
"gru-kevin", "veyron-jaq" and "nyan-big" are part of the mainline LAVA
releases, so relatively little configuration is required for them. Installing
LAVA and managing devices in a lab is a whole topic which goes beyond what this
blog post is about; a good place to start is the [LAVA
documentation](https://validation.linaro.org/static/docs/v2/device-integration.html).

In a nutshell, the power can be controlled with commands of this kind:

```
dut-control cold_reset:off
dut-control cold_reset:on
```

and the console is available over USB. For example, there are several
Chromebook devices booting with Depthcharge in the Collabora LAVA lab (they use
the firmware binaries listed above):

rk3288-veyron-jaq devices rk3399-gru-kevin devices tegra124-nyan-big devices In
order to be able to use dut-control without a Chrome OS build environment, and
to automatically bring up the device connections,
[servod-tools](https://gitlab.collabora.com/chromium/servod-tools) can be used
in conjunction with
[hdctools](https://gitlab.collabora.com/chromium/hdctools). Installing and
using these still requires a fair amount of manual configuration, which could
be a topic for a future blog post.


## Chromebooks on kernelci.org

The main objective with doing all this was to be able to test the mainline
Linux kernel on these Chromebook devices via kernelci.org. The same devices
listed above have all been enabled, baseline results can be found here:

* [rk3288-veyron-jaq baseline tests](https://linux.kernelci.org/test/plan/id/61135484b7c3141deab1366a/)
* [rk3399-gru-kevin baseline tests](https://linux.kernelci.org/test/plan/id/611353a30be08a74e8b13670/)
* [tegra124-nyan-big baseline tests](https://linux.kernelci.org/test/plan/id/611350a8b8b83fbc0fb13669/)

Now that the basic infrastructure to run tests is available, we're working on
adding many more functional tests to cover various areas of the Linux kernel
via user-space APIs - but that's for another blog post.

Stay tuned!
