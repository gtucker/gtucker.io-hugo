---
author: Guillaume Tucker
title: First year in Freelance
date: 2025-02-27
tags: [Business, Software, Energy]
description: >-
    Looking back, looking ahead
images: [/image/fosdem-2025-cookies.jpeg]
---

You may be starting off in freelance yourself or just curious to know what it
means.  Here's the story of my first year with some lessons learnt and a
glimpse into what might be coming next.

It was sunny this February at FOSDEM as per the picture above.  It reminded me
of when I was there last year during my early days as self-employed and the
long trail of events I have been through since then.

## Ready, Set...

It all started when [I left Collabora](/posts/2023-11-18-beyond-open-source) at
the end of 2023.  While I had been contemplating such a move for a while, there
was no way I could really find the time and space to be ready for it while
having a full-time job.  Building a business model and getting all the
necessary things lined up are simply part of freelance itself and can't be
completed beforehand.  The key thing is to become convinced of your potential
and how it should allow you to strive in a particular environment.  It is also
crucial to take some risk mitigation measures and prepare the ground:
trainings, savings, fallback scenarios, side jobs, living cost reduction
alternatives etc.

Another very, very important goal is to properly wrap up any activity you need
to stop.  In my case, I had a long notice period and I was working on KernelCI
which is an open-source Linux Foundation project.  As such, I first focused on
internal handover duties with my colleagues and then had no real deadlines left
for completing the community side of things.  In practice that meant making my
own timeline for transferring responsibilities and maintainer roles to other
people as well as helping find a new Chair for the [KernelCI
TSC](https://docs.kernelci.org/org/tsc/).

## Go!

Day 0 is when any previous employment ends and a new journey begins.  The
amount of time and mental space it instantly brings can be a blessing and a
curse.  Unless you have already found some early customers, the apparent calm
could easily become a storm of anxiety.  Although everyone will get a different
experience from it, I believe the best approach is to put healthy routines in
place and keep moving forward.  There's a fine line between taking your time to
reflect constructively and becoming idle.  It's a great challenge and an
opportunity to expand your self-motivation which can only make you stronger.

Each individual will need a tailored solution.  I found it to be a good time to
brainstorm and gather as many ideas as possible about places to go next.  While
planning won't make any of it actually happen by itself, it's a productive way
of being prepared for when an unexpected opportunity arises.  Having worked for
several startups before, I felt some commonalities in the mindset in terms of
being very reactive and flexible.  Still, the aim is a different one since it's
bound to be a solo journey and the scope can be much wider than having a
laser-focused pitch to get a product or business idea over the line.  And
finally, to balance things out against free-flowing inspiration, it's crucial
to have checklists to go through and keep making concrete progress every single
day.

Let's take a quick look at a handful of items that paved my way here:

### 1. Admin

An obvious but necessary initial step is to adopt the most appropriate
self-employed status for the work.  This will vary widely between countries and
depends on the nature of your business model.  In my case I opted for the
[micro-entreprise](/posts/2024-05-06-micro-entreprise) framework which is a
common choice in France with simplified procedures.

Other admin duties include bookkeeping, insurance, banking, taxes, having a
dedicated company address to receive mail when working from home and lots of
other small things along those lines.  Overall, I don't think there should be
any major hurdle or hard decisions to take in doing this.  It can be quite time
consuming at first, then it should become easier by putting a good process in
place to avoid repeating the same manual tasks over and over.  Getting
professional advice and support is money well worth spending too.

### 2. KernelCI handover

Throughout the first quarter of 2024, I had to regularly keep in touch with the
KernelCI community and attend meetings in order to complete the transition.  I
eventually opted to simply remain a member of the TSC to contribute my past
experience and bring some variety following my move.

To conclude my direct involvement in the project, I was invited to give an
online talk as part of the Linux Foundation Mentorship sessions: [KernelCI
Travel Guide
2024](https://www.linuxfoundation.org/webinars/kernelci-travel-guide-2024).
This was a good way to assess the project's situation and for me to consider my
next steps as an independent contributor.  I then ended up making a
[fork](https://gitlab.com/gtucker.io/renelick) of the code to continue pushing
its design further with a more general-purpose automation tool - more on that
in an upcoming blog post!

### 3. Energy

When I started my MEng degree in electronics, I had to complete a survey which
included a question about future work aspirations.  I already knew I wanted to
work in the energy sector and especially in sustainable ways to generate
electricity.  There were of course already a few things going on in this field
about 20 years ago but it was mostly research or anecdotal projects.  The
technology wasn't there yet as it hadn't been receiving that much investment
and attention.  Things have changed drastically since then with the electric
vehicle revolution and a raising awareness of the climate emergency.

This is when having a chance to make a career turn can be useful for realigning
the nature of your work with your true aspirations.  It might feel like
something you should have embraced earlier but it's never too late to make a
change.  Years of experience in any domain will always be valuable as a unique
personal strength.  In my case, I could refresh my power electronics academic
knowledge and build upon the open-source expertise I have acquired.  The
[Energy devroom](/posts/2024-02-05-fosdem) at FOSDEM 2024 was a major event for
the industry but also on my personal journey as I could see new horizons
opening up and engage with an emerging community.  It simply joined all the
dots.

### 4. Consultancy

Leaving all the new avenues I may be able to explore on one side, the
overarching goal is to provide consultancy services.  As such, I felt I needed
to rely on the full range of knowledge I had gathered through life to put all
the odds on my side.  Signing the first contract is a fantastic milestone as
it's a tangible realisation of what was once nothing but a faint, abstract
idea.  This is also when the vulnerability of leaving the comfort of a nice and
stable job in a privately-funded company brings true gratification.

Keeping a sustained source of income is the real deal though as that proves the
viability of the whole enterprise.  I can't stress enough how grateful I am to
have been given such a wonderful chance to achieve this.  Without further ado,
let me introduce you to...

## Source.dev

<div style="float: right; padding: 0 0 0 12px">

![source.dev logo](./source-logo.png)

</div>

Some might say that timing is everything.  I would argue that it's akin to how
a photographer would dedicate years to eventually know how to spontaneously
take a truly great snapshot at an unpredictable time and place.  Stepping into
self-employment with the agency that it brings automatically led me to seize
the day in unexpected ways.  That is when I renewed contact with
[Serban](https://www.linkedin.com/in/serban-constantinescu/), a former
colleague of mine from my days at ARM who was about to launch a startup with
[David](https://www.linkedin.com/in/davidbrazdil/) whom he had met at Google.
It was Summer 2024 and the beginning of [Source.dev](https://source.dev).

In short, the main goal is to facilitate Android product development and
long-term support for device ecosystem companies, from OEMs to chip providers.
This also enables them to deal with new regulations aiming to extend consumer
products' lifespans.  It is all made possible thanks to the unique expertise
accumulated over the years by the Source.dev co-founders.

Had I not published a blog post about my professional move, I probably would
never have appeared on their radar.  Also, I privately hold the belief that
what might seem like an anecdotal, long-forgotten event played a key role in
this.  Serban once asked me to port some micro-benchmarks from Javascript to
Java after randomly bumping into me at a Linaro Connect conference about 10
years ago.  I completed this for an MD5 use-case on the same day with my
lightweight ARM Chromebook in a hacking room and it definitely had a positive
impact on our mutual trust.  Spending several years bringing KernelCI to
maturity in the meantime obviously also helped a lot.

All these things are intertwined as the world is a continuum.  Regardless of
the actual causes, the outcome is that I am now contributing my fair share to
this high-profile customer with an ongoing part-time contract.

## Momentum

Once things have been set into motion, it's all about fostering that momentum
and being always on the lookout for new developments.  Having a stable revenue
with strong customer relationships makes it possible to carve out some time and
space for this.  It's also a way to reconnect with former colleagues and extend
your network.  It helps to feel part of something bigger since humans are
social animals and as a result, it will naturally generate new business leads
too.

I am now looking forward to discovering what lies ahead in 2025.  Among all the
hardship throughout the world and global uncertainties, it's important to be
able to focus on what matters at an individual scale.  Finally, feel free to
reach me via [email](mailto:gtucker@gtucker.io) or
[LinkedIn](https://www.linkedin.com/in/guillaume-tucker/) as I would love to
know your thoughts and your own stories too.
