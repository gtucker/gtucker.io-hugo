---
author: "Guillaume Tucker"
title: "Trumpet reflection #2: All about the Lips"
date: 2022-02-28
tags: ['Music', 'Trumpet']
description: "What the buzz?"
images: ["/image/lips.jpeg"]
---

A trumpet is a resonator for vibrating lips.  Unlike say, a saxophone
which relies on a reed to produce the initial sound, with a trumpet
we're on our own.  When playing a high C note (Bb1 in concert pitch),
a small area of the player's lips are literally moving back and forth
932 times per second.  Natural selection has arguably shaped how
humans can now communicate using our voice to produce a multitude of
sounds, but I'm not sure individuals who could vibrate their lips
faster had a greater chance of survival.  Therefore, it's not really
intuitive.  However, we're a playful species and we can learn weird
tricks - such as buzzing in a horn.  With enough determination, anyone
can do it!

## Tips for lips

Here's a bunch of tips I've collected over the past few years.  Some
are well known facts whereas others are observations based on my
personal experience.  Every individual is different, so the key thing
is to realise when something has some impact over your lips or playing
in general and how to best deal with it.

### Beeswax

When I was eleven, I often had chapped lips with some kind of burning
feeling.  At the time, I wasn't even aware it was related to playing
the trumpet since it could happen many hours later.  Since then I've
learnt that sunshine, wind or even just the air coming out of my nose
caused lips to dry which made the problem worse.  At first I tried to
put some beeswax-based lipbalm _before_ playing, but that came with
some pretty bad side effects: slipping mouthpiece, particles of wax in
the horn, and worst of all it would add some weight to the lips and
slow them down.  It might be OK for a didgeridoo player (wax is
commonly used on this instrument) but not for trumpet which is
designed to produce high and sharp tones.  Using it _after_ playing
was better, but sometimes it wasn't needed or I might have to remove
it later if I needed to play again.

The bottom line is that it has to be used as little as possible,
mostly before being exposed to wind or sunshine for a substantial
amount of time or when already feeling some dryness, and only on the
rim of the lips.  It has saved me some pain but also provided a
protective layer for the skin to let it regenerate itself.  All traces
of beeswax or other kinds of lipbalm should probably be thoroughly
removed before playing though.

### Flapping

Here's a very well known thing: when exercising a muscle, lactic acid
builds up as a byproduct which then leaves it weakened.  This goes
away quicker by facilitating blood flow, typically by stretching the
muscle.  Now, how do you do that with your mouth?  One way is to
"flap" the lips, or basically buzz at a very low frequency.  A bit
like a horse, or as some kind of disconcerted reaction when someone
tells you something rather obnoxious.  I often do this while having
breaks between warming-up exercises, and I believe it helps after
playing for a long period of time too.  Ultimately, it should become
second nature to feel whenever it's needed.  And I don't think it can
hurt, at least not yourself - just be mindful of others who might be
crossing the stream of your droplets!

### Drinks

Water is the only indispensable drink.  It's also a very safe and
useful one to rely upon when playing the trumpet.  Being well hydrated
in general helps with being in good physical shape, and it regulates
the temperature of the lips.  However, pretty much every other kind of
drink can potentially have tricky side-effects so it's good to know
them as you might find yourself playing in various environments:

* Tea & coffee: If it's too hot, it can obviously burn the lips or the
  tongue.  This is even more critical when playing the trumpet as it
  can make the skin more sensitive, so make sure the temperature is
  well under what normally feels like "burning hot".  More subtely,
  these drinks can contain some particles that will stay in the mouth
  for a little while and require saliva to digest and evacuate them.
  An increased amount of saliva means more of it will also go into the
  horn and produce that dreaded crackling noise.  It's probably good
  to drink something warm when playing outdoors in a cold weather, but
  waiting for maybe one minute before playing should help.

* Beer, Prosecco and other carbon bubbles: Of course, they cause gas
  to be released in the digestive system which might result into some
  annoying pressure or even some surprising tremolo effect when losing
  control over it.  Avoid at all costs, including lukewarm stale
  English real bitter ale.

* Red wine: It contains tanin, which can notoriously deposit on the
  lips and form some pretty tough stains.  They may get in the way of
  higher notes and loudness as the skin becomes more rigid and
  heavier, so it takes more effort to make it vibrate (basically, a
  mechanical low-pass filter).  I think it can also cause some
  acidity, which isn't nice either for the skin nor for the brass of
  the horn.  It can however dissolve easily using the tongue.

* White wine: Although harder to find of decent quality in bars, it's
  much safer than red wine.  I think the only issue is probably also
  the acidity.  The same thing applies to lemon juice etc.  Drinking
  water before playing is always a good idea anyway.

* Distilled alcohol: Whisky, Vodka etc. when used straight are likely
  to cause some dryness on the lips or weaken the mucus in the mouth.
  So I don't think they help, but at least they don't have any other
  major side-effects.  I mean, for the particular subject at hand.

* Cocktails: Well, this varies greatly - see the list above and make
  your own judgment based on the ingredients.  Above all, avoid
  playing after having had a margarita with salt on the rim of the
  glass as that's really corosive for brass and it causes dryness of
  the skin.  Gin & Tonic is probably the safest thing to drink in a
  bar before playing, and a bit of pure gin can help remove any
  remaining traces of lip balm or whatever.  Tonic water isn't very
  gassy and a bit of sugar might help stay alert (eating too much food
  can divert a lot of blood and may hinder your capacity to blow some
  air into the instrument, but that's a topic for another post).

### Moustache

Needless to say, some humans don't have this hairy issue.  But for
those like me who do, I've found that the important part is really to
trim the edge so that the contour of the mouth appears neatly.  This
can be done very easily with a fine pair of scissors.  Once you follow
this rule, there's basically no facial hair in contact with the
mouthpiece so it's all fine.  In fact I've never really been forced to
stop playing due to some moustache syndrome.  It's just annoying when
it gets in the way and it's not that hard to avoid.

## Under pressure

A common beginner's mistake is to apply too much mechanical pressure
on the lips, by pulling the horn.  This might be some kind of
intuitive gesture to help with higher notes that are hard to play, but
it's really bad practice.  You might have to stop playing for a few
days if any pain or visible damage appears.  Instead, what's important
is to learn how to produce higher air pressure and find the optimum
position and angle for the air flow to enter the mouthpiece.

To play louder, I like to try and move the horn away from my mouth and
blow air at higher pressure to keep the sound going.  For higher
notes, this also applies but there are additional techniques with the
shape of the tongue and mouth to accelerate the air flow.  It would
take many more words to try to describe how that works.  To be
continued...

<small>Photo by [Kaveh](https://www.flickr.com/photos/92388714@N00/2793914454)</small>
