---
author: "Guillaume Tucker"
title: "Trumpet reflection #1: In the Beginning"
date: 2021-10-18
tags: ['Music', 'Trumpet']
description: "Going back to square one, over and over again"
images: ["/image/trumpet-banner.jpeg"]
---

In the beginning, there was silence: a cold, metallic tubular object with an
exponential shape.  This is what I would inevitably go back to every single day
before playing the instrument.  Surrounding me is an atmosphere which will be
the vehicle for any sound.  This is where things start to get complicated as
its properties vary greatly according to the situation.  Then acoustics come
into play.  Unlike a listener or a microphone, the player mostly hears indirect
sounds from behind the instrument.  But wait, which player?

## The Trumpet Player

Obviously, while the local environment matters a great deal, the main source of
varying factors and randomness is the person about to play the instrument.
Conversely, that same person should also be able to adapt to any given
situation.  Practising always in the same conditions helps as it provides a
reference point.  The player can then establish a warming-up procedure to keep
recreating an optimal state and ultimately be ready to perform anywhere.
Sleep, food, mood and health in general have a direct effect on the initial
ability to produce a sound, let alone to play anything more elaborate or
improvise.  As each person is different and keeps evolving, it is a
never-ending challenge.

## Square One

Warming up basically equates to learning again how to play the instrument from
scratch, in fast-forward.  That is probably true for music in general and many
other activities, but what about the trumpet specifically?  Every aspect of it
would deserve a full study: breathing, posture, muscles, mental focus...  The
key thing to remember is that cutting corners will result in pain, low
endurance, poor tone and inaccurate pitch control among other things.
Experience will unveil more effective ways to repeatedly go from square one to
however advanced you might be rather than directly attempt to jump there with
the first note of the day - and miss.  At least, this is what I have found to
be the most important thing to keep in mind.  If I feel like I'm not performing
as well as usual, it usually means that I haven't warmed up well enough.

## Reflections

This is the first post in a projected series to reflect upon the journey I
resumed five years ago as a trumpet player.  The instrument offers me
alternative perspectives upon many things, so I might touch on other subjects
too.
