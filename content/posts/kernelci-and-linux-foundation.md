---
author: "Guillaume Tucker"
title: "KernelCI and the Linux Foundation"
date: 2019-10-28
tags: ['Software', 'Linux', 'KernelCI', 'Collabora']
canonicalUrl: "https://www.collabora.com/news-and-blog/news-and-events/a-new-home-for-kernelci.html"
description: "A KernelCI developer's view on the project joining the Linux Foundation"
images: ["/image/kernelci-logo.png"]
---

As announced today at the Open Source Summit in Lyon (see the official [press
release](https://www.prnewswire.com/news-releases/distributed-linux-testing-platform-kernelci-secures-funding-and-long-term-sustainability-as-new-linux-foundation-project-300945978.html)),
the [KernelCI project](https://kernelci.org) which powers
[linux.kernelci.org](https://linux.kernelci.org) with automated testing for the
upstream Linux kernel has now **joined the Linux Foundation**. This is the [new
home](https://foundation.kernelci.org/) it has finally found after sailing
through uncharted waters for over five years.

## What does it actually mean for the project?

Given the huge scale at which the Linux kernel is being used, achieving
comprehensive test coverage for it is an incredibly challenging goal. Based on
the open source philosophy principles, KernelCI's distributed architecture
makes this possible by enabling the whole kernel community to collaborate
around a single **upstream CI system**. Becoming part of the Linux Foundation
will let the project flourish and become in turn integral part of the Linux
kernel development workflow.

Some actual results of this move can already be seen with the **new common
database** for storing test results from KernelCI and other related projects
that share a common goal such as Red Hat's CKI and Linaro's LKFT. It is an
experimental step towards expanding KernelCI in a modular way to interact with
other existing systems. There are as many different ways to test the kernel as
there are use-cases for it, and many types of specialised systems to cover: a
CPU architecture such as
[0-day](https://01.org/lkp/documentation/0-day-test-service), a Linux
distribution such as [CKI](https://cki-project.org/), a range of reference
platforms such as [LKFT](https://lkft.linaro.org/)...

## What happens next?

This is only a new beginning, many things are yet to come with **many decisions
to be made**: how to use the budget available from the membership scheme, how
to make the infrastructure more sustainable and scalable, how to compromise and
address the needs of all the members joining the project... Answers to all
these questions are likely to appear as the coming months and years unfold.

One thing we can be sure of is that there is no reason for the current
development plans to stop or be impacted in any way - on the contrary. There is
indeed a strong need to extend test coverage and the capabilities of KernelCI
at large, with a **huge potential** to improve upstream kernel development as a
direct result. Becoming part of the Linux Foundation should be about
facilitating progress in this direction above all.

## What does Collabora plan to do?

Collabora has been involved with KernelCI since almost the beginning, providing
some of the servers running the kernelci.org services and a large [LAVA test
lab](https://lava.collabora.co.uk/). We've also become the biggest contributor
to KernelCI development with myself as de facto maintainer for the core
repositories on Github and in charge of the weekly kernelci.org updates.

Among all the things we have done in the **last couple of years**, these are
probably the most significant ones:

* Enabled automated bisection for boot regressions
* Added v4l2-compliance test suite and a few more minor ones
  (suspend/resume...)
* Enabled "Depthcharge" bootloader in LAVA to run [KernelCI tests on
Chromebooks](../chromebooks-depthcharge-lava-kernelci)
* Portable command-line tools and YAML-based configuration to perform all the
  KernelCI steps in any environment

Going forward, here are some clear items on the project's **current roadmap**
which will carry on full steam ahead with a growing team of developers:

* Further improve handling of test results: web interface, email reports,
  regression tracking
* Extend coverage of test suites:
  [kselftest](https://www.kernel.org/doc/html/latest/dev-tools/kselftest.html),
  [LTP](https://github.com/linux-test-project/ltp),
  [KUnit](https://kunit.dev/third_party/kernel/docs/),
  [igt-gpu-tools](https://drm.pages.freedesktop.org/igt-gpu-tools/test-programs.html)
  and many more
* Deploy new bisection tool to cover complex test suite results
* Improve [modular core
  architecture](https://docs.google.com/document/d/15F42HdHTO6NbSL53_iLl77lfe1XQKdWaHAf7XCNkKD8)
  to enable inter-operability between the various elements that form the
  overall KernelCI pipeline

Then like with every open source project, other contributors will also add more
features, enable new platforms in the global test hardware pool or bring new
ideas. The combined efforts of all parties is what makes the project unique and
mature enought to **reach its ultimate goal**. We will make sure we continue to
be among the top active players within the growing KernelCI community, as a
founding member of the Linux Foundation project as well as with myself being
both a maintainer and on the technical steering committee.
