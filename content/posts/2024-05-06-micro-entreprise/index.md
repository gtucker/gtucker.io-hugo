---
author: Guillaume Tucker
title: micro-entreprise
date: 2024-05-06
tags: [Business, Software, Energy]
description: >-
    Early morning vertical solo climbing
images: [/image/climbing-snail.jpeg]
---

Spring is here.  After a long wave of April showers, I'm now very pleased to
announce that the garden plot I was allocated a month ago is going well and a
few shoots are starting to show up.  Equally importantly, I've finally
established the framework for my upcoming work initiatives.  It's a steep
climb, almost vertical, although a very rewarding one.  Steady progress
eventually leads to new heights and can open new horizons.

# Overview

Essentially, *micro-entrepreneur* is a French flavour in the self-employed
category with [simplified
procedures](https://entreprendre.service-public.fr/vosdroits/F37398).  So I'm
open for business, officially since 1st May 2024.  That's for the status, but
what about the actual work?

First of all, I'll carry on doing **open-source software** development in the
foreseeable future.

Secondly, I'm kickstarting some R&D in the field of **renewable energy** which
should start bearing fruit over the coming months.

Lastly, a few extra activities may also come into play such as designing
**hardware kits**, running workshops and education.

I'd like to take the opportunity here to include a section in French since I
now live in [Tours](https://en.wikipedia.org/wiki/Tours).  It's basically a
summary of the English parts.

# En Français

<div class="float-hack-right">

![Jardin familial](./jardin-familial.jpeg)

*Jardin familial / Allotment plot*

</div>

Après avoir vécu pendant quinze ans au Royaume-Uni, j'ai découvert beaucoup de
choses changées à mon retour en été 2022.  Je ne conaissais pas
[ameli.fr](https://ameli.fr), j'ai dû faire beaucoup de démarches souvent
lentes, manuelles et via divers organismes isolés.  J'ai pourtant eu la chance
de pouvoir garder mon emploi à distance avec mon
[employeur](https://collabora.com), ce qui m'a grandement simplifié la tâche.
Malgré tout, j'ai finalement décidé de franchir le cap et commencer ma nouvelle
activité en freelance ce 1er Mai 2024.

Il y a de quoi écrire de longs articles sur les différences de cultures ainsi
que sur les procédures administratives.  Je me limiterai ici au fait que le
prix du vin en France est comparable à celui de la bière en Angleterre, et à
une liste des étapes principales pour démarrer une entreprise individuelle:
choix du statut, solution de domiciliation, définition des activités & business
plan, formulaires INPI, assurance RC Pro, compte bancaire...

Mais venons-en au sujet principal.  Prendre le temps de faire une pause
profesionnelle permet de revoir ses priorités et redonner du sens à son
activité.  C'est aussi l'occasion de rétablir un équilibre avec sa vie
personelle, surtout après un changement important de situation.  Quelles
conclusions en ai-je donc tirées?

Il me semble qu'être prêt à s'adapter aux changements rapides est devenu la
priorité.  Cela se traduit dans mon cas à choisir certains domaines et y
déveloper une base pour catalyser des opportunités futures - dont certaines
déjà en vue.  Concrètement, après vingt années de dévelopement logiciel et
particulièrement du [libre](https://fr.wikipedia.org/wiki/Logiciel_libre), il
est naturel que je poursuive dans cette voie: noyau Linux, devops, Web3...
Entre temps, une passion qui n'a pas cessé de m'animer depuis mes études
d'ingénieur en électronique se retrouve maintenant en plein essor: les énergies
renouvelables.  C'est donc mon second terrain d'action, plein de potentiel
comme j'ai pu en témoigner à [FOSDEM](/posts/2024-02-05-fosdem).

Une autre façon de valoriser son expérience est de la partager, en particulier
par le biais de l'enseignement: interventions en milieu universitaire,
ateliers, tutoriels...  C'est aussi un bon moyen en tant que travailleur
indépendant pour créer des connexions et être actif dans une communauté, tant à
l'échelle locale qu'à distance.  Un atout supplémentaire dont je bénéficie est
la possibilité de le faire en anglais.

<div class="float-hack-clear" />

# Strategy

Of course, it's hard to predict exactly how things are going to pan out.  The
overall strategy at this stage is to focus on a number of particular areas to
prepare the ground for longer-term opportunities - with a couple already in
sight.

Taking the time to slow down during a career break makes it possible to reflect
upon personal priorities and renew a sense of purpose.  This is true for every
aspect of someone's life such as family and health, but let's focus on work for
now.  Seven years ago, a similar approach led me to invest fully into
open-source software when I joined
[Collabora](https://www.collabora.com/news-and-blog/).  The stakes are very
high today with so many disruptive events reshaping the world.  Renewable
energy is finally receiving massive amounts of interest while artificial
intelligence is also taking off exponentially.  How to deal with such
macroscopic things as an individual?

## Software

Given my past experience in open-source software consultancy, it's only natural
that I keep this momentum going.  The big difference now is that I can open up
new avenues.  To start with, I'll resume actual Linux kernel development and
most likely extrapolate from some of the devops automation work I did around
[KernelCI](https://kernelci.org) to other use-cases.  I'm also fully learning
the [Rust](https://www.rust-lang.org/) language, diving deeper into machine
learning technicalities and investigating Web3 technologies such as
[Polkadot](https://polkadot.network/).

Software is everywhere though, and the next main application domain I want to
focus on is energy.

<div class="float-hack-left">

![Photovoltaic harvesting devkit](./pv-devkit.jpeg)

*Photovoltaic harvesting devkit*

</div>

## Energy

Exactly twenty years ago, in 2004, I was answering a particular question while
filling an entry questionnaire for my
[MEng](https://en.wikipedia.org/wiki/Master_of_Engineering) electronics degree
about career aspirations.  In my case, it was all to do with renewable energy
and contributing to a more sustainable future.  While it was probably seen more
as a niche or nerdy activist idea back then, we've now come a long way.

It's so exciting to be able to put into practice all my dormant hardware skills
on top of the software ones: power electronics, automation, sensors, signal
processing...  And since open-source concepts have now become mainstream too,
as witnessed at the [FOSDEM Energy](/posts/2024-02-05-fosdem) devroom there's a
lot to be done both in software and hardware to reinvent this century-old
industry with open standards.

Likely topics for some future blog posts are going to be micro-grids,
electrical substations, machine learning applied to energy production and
transport, software tooling at every level of the stack and hardware kits.  But
that's enough buzzwords for today.

<div class="float-hack-clear" />

## Connecting the dots

Another important aspect of freelancing is communicating and engaging with
social networks.  While working remotely, some typical means to achieve this
would be taking part in conferences and events which can be online or
face-to-face, writing articles and creating educational material such as
tutorials.

Other ways to reach out to new communities would of course be contributing
technical value (code, design, discussions...) but also teaching and running
workshops.  One particular way for me to put my experience to good use in
France would be via meetups in English around tech topics, and lectures.

As always, you're more than welcome to get in touch and share any thoughts you
might have via [email](mailto:gtucker@gtucker.io).

Speak soon!
