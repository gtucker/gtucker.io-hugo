---
author: "Guillaume Tucker"
title: "First post"
date: 2017-08-11
tags: ['Software', 'Music']
description: "What made me start this blog."
---

Hello! This is my first post here, about the blog itself. I took a bit of time
after attending DebConf 2017 to set this up as a way to share what I believe to
be important. It should mostly revolve around free and open source software, as
well as free music and anything that makes our society open and respects the
freedom of each individual.

Although free software is now everywhere and expanding continuously, from small
gadgets to supercomputers, it's fair to say that few people seem to actually
appreciate its deep implications. The business models built around open source
and free software are not obvious to many people, the revenue streams are not
based on selling copies of a proprietary product. The actual product sold
instead is the source code, which is what developers get paid to create and
maintain. This is then available for anyone to use and build other products
with, and contribute back to the source code. The possibilities are endless, so
instead of wasting efforts reinventing what others have already done, the focus
is put on progressing existing things in a common interest. And anyone can take
part, learn and contribute.

This also leads to many fascinating topics beyond software engineering, such as
the actual growth of available wealth in society and how measuring it greatly
depends on one's point of view. Or how people can work together and express
their full potential in life by not having artificial restrictions imposed upon
them.

So all this kind of stuff, and just plain fun things!
