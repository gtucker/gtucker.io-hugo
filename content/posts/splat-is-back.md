---
author: "Guillaume Tucker"
title: "Splat is back"
date: 2018-08-01
tags: ['Music', 'Software', 'verdigris.org']
description: "Splat is a project to generate music and audio files using Python."
---

Get the code from
[gitlab.com/verdigris.mu/splat](gitlab.com/verdigris.mu/splat).

## What is Splat?

<p><img alt="Splat!" src="/image/splat-foot.png" style="float: left; padding: 0
20px 10px 0" /></p>

**Splat is a program** to generate some audio data which you may call
music. It's written in Python to make it easy to use, and it's distributed
under the GNU LGPL v3 free software license. In fact, only half of the code is
Python. The lower-level parts are written in C to strike a **balance between
features and speed**. There's even a fast mode with a smaller sample size and
optimised functions with SSE and NEON intrinsics.

Splat is **not** a program to generate **live** audio in real time, at least
not at the moment. So it's better suited to the studio than to the stage.

You can hear a few examples of what can be done with it on
[SoundCloud](https://soundcloud.com/verdigrix/sets/splat-1).

## Moved permanently to gitlab.com

After a quiet period of a few months, it's now being revived with some new
ideas and a new home.

Splat used to be hosted on
[github.com](https://github.com/verdigris/splat). However, there were a few
issues with that. First of all, although it is the home of many open source
projects, the Gitlab platform itself is not open source. Then continuous
integration is not directly part of Github, so out of the few possible plug-ins
available I initially opted for [Semaphore
CI](https://semaphoreci.com/). However I quickly hit the limitations of this
design as I was adding support for Python 3: since the tests are defined
outside of the project, they had to be either compatible with Python 2.7 or
Python 3. Also, it was hard to justify the overhead associated with managing an
extra account for the CI part given the rather limited capabilities of the
system.

So Splat has now moved to [gitlab.com](https://gitlab.com/verdigris.mu/splat),
with [integrated CI](https://gitlab.com/verdigris.mu/splat/pipelines). This
automatically builds the code, runs tests and generates the [PDF
documentation](https://gitlab.com/verdigris.mu/splat/-/jobs/86826171/artifacts/file/doc/_build/latex/Splat.pdf)
for every revision on every branch. Further steps would be to also run
benchmarks and keep track of the scores, and also test on ARMv8 to cover the
NEON build flavour - but that's for a bit later, and another blog post.

## Highlights

On the technical side of things:

* Splat has now been ported to Python 3.
* There's also support for using mmap for memory allocation of large data
  fragments and intermediate files.
* The integrated splat make command is very useful to generate parts of an
  audio composition independently and then mix them together. It's even better
  when combined with mmap.

On the more musical side of things:

* There's some work-in-progress to create a library of sound generators with
  "knobs", or simple parameters. This hasn't been merged yet but there's an
  initial merge request #4.
* Another work-in-progress is to extend the harmonic-based scales, using just
  intervals with tonality context to enable modulation and historical things
  such as the Pythagorean scale. It's more at the research stage at the moment,
  so stay tuned!
